import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_getx/consutu.dart';
import 'package:flutter_getx/controller/detail_controller.dart';
import 'package:flutter_getx/view/widgets/app_icon.dart';
import 'package:get/get.dart';
import 'package:flutter_getx/view/widgets/app_text_bold.dart';
import 'package:flutter_getx/view/widgets/app_text_normal.dart';


class DetailPage extends StatefulWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  List imgs = [];

  _readDate() async {
    await DefaultAssetBundle.of(context)
        .loadString("assets/json/img.json")
        .then((s) {
      setState(() {
        imgs = json.decode(s);
      });
    });
  }

  @override
  void initState() {
    _readDate();
    super.initState();
  }

  final DetailController _controller = Get.put(DetailController());

  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      body: Container(
        color: kBackgroundRecentContest,
        child: Stack(children: [
          // home
          _myBackHome(),
          // information
          _myInformation(width: width),
          // background white
          _backgroundWhile(height: height, width: width),
          // posts
          _myPosts(height: height, width: width),
          // total participants
          _myTotalParticipants(),
          // image total participants
          _avatar(imgs: imgs),
          // add to favorite
          Positioned(
              top: 490,
              left: 20,
              child: Row(
                children: [
                  Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: kButtonColor),
                      child: InkWell(
                        onTap: () {
                          _controller.favCountor();
                        },
                        child: Icon(Icons.favorite_border,
                            color: Colors.white),
                      )),
                  const SizedBox(
                    width: 10,
                  ),
                  TextNormal(
                    text: "Add to favorite",
                    color: kButtonColor,
                    size: 18,
                  ),
                ],
              )),
          // Positioned(
          //   top: 550,
          //   child: Expanded(
          //     child: SingleChildScrollView(
          //       scrollDirection: Axis.vertical,
          //       child: Container(
          //           padding: EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding,),
          //           child: GridView.builder(
          //             itemCount: images.length,
          //             gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          //                 crossAxisCount: 2,
          //                 crossAxisSpacing: 4.0,
          //                 mainAxisSpacing: 4.0),
          //             itemBuilder: (BuildContext context, int index) {
          //               return Image.network(images[index]);
          //             },
          //           )),
          //     ),
          //   ),
          // ),
        ]),
      ),
    );
  }
}

class _avatar extends StatelessWidget {
  const _avatar({
    Key? key,
    required this.imgs,
  }) : super(key: key);

  final List imgs;

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      for (int i = 0; i < imgs.length; i++)
        Positioned(
          top: 430,
          left: (20 + i * 35).toDouble(),
          width: 50,
          height: 50,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                image: DecorationImage(
                    image: AssetImage(imgs[i]['img']),
                    fit: BoxFit.cover)),
          ),
        )
    ]);
  }
}

class _myTotalParticipants extends StatelessWidget {
  const _myTotalParticipants({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 400,
        left: 20,
        height: 50,
        child: RichText(
            text: const TextSpan(
                text: "Total Participants ",
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 20,
                    color: Colors.black),
                children: [
              TextSpan(
                  text: "(11)", style: TextStyle(color: kButtonColor))
            ])));
  }
}

class _myPosts extends StatelessWidget {
  const _myPosts({
    Key? key,
    required this.height,
    required this.width,
  }) : super(key: key);

  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: height * 0.28,
      left: 0,
      width: width,
      height: 180,
      child: Container(
        margin: const EdgeInsets.only(
            left: kDefaultPadding, right: kDefaultPadding),
        width: width,
        height: 180,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: kWhiteTextColor,
            // ignore: prefer_const_literals_to_create_immutables
            boxShadow: [
              const BoxShadow(
                  blurRadius: 20,
                  spreadRadius: 1,
                  offset: Offset(0, 10),
                  color: kGreyTextColor)
            ]),
        child: Container(
          margin: const EdgeInsets.only(
              left: 20, top: 20, bottom: 20, right: 20),
          child: Column(
            children: [
              Container(
                  child: Row(
                children: [
                  TextBold(
                    text: Get.arguments['title'],
                    size: 24,
                  ),
                  Expanded(child: Container())
                ],
              )),
              const SizedBox(height: 5),
              Container(
                width: width,
                child: TextNormal(
                  text:
                      Get.arguments['text'],
                  size: 16,
                  color: kGreyTextColor,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              const Divider(
                thickness: 1.0,
              ),
              const SizedBox(
                height: 3,
              ),
              Row(children: [
                Expanded(
                  child: Container(
                    child: Row(children: [
                      AppIcon(
                          source: "assets/icon/clock.svg",
                          height: 18,
                          width: 18),
                      const SizedBox(
                        width: 5,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextBold(
                            text: Get.arguments['name'],
                            size: 12,
                          ),
                          SizedBox(
                              child: TextNormal(
                            text: "Deadline",
                            size: 10,
                            color: kGreyTextColor,
                          )),
                        ],
                      )
                    ]),
                  ),
                ),
                Expanded(
                  child: Container(
                    child: Row(children: [
                      AppIcon(
                          source: "assets/icon/dollar.svg",
                          height: 18,
                          width: 18),
                      const SizedBox(
                        width: 5,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextBold(
                            text: Get.arguments['prize'],
                            size: 12,
                          ),
                          SizedBox(
                              child: TextNormal(
                            text: "Prize",
                            size: 10,
                            color: kGreyTextColor,
                          )),
                        ],
                      )
                    ]),
                  ),
                ),
                Expanded(
                  child: Container(
                    child: Row(children: [
                      AppIcon(
                          source: "assets/icon/star.svg",
                          height: 18,
                          width: 18),
                      const SizedBox(
                        width: 5,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextBold(
                            text: "Top Level",
                            size: 12,
                          ),
                          SizedBox(
                              child: TextNormal(
                            text: "Entry",
                            size: 10,
                            color: kGreyTextColor,
                          )),
                        ],
                      )
                    ]),
                  ),
                ),
              ]),
            ],
          ),
        ),
      ),
    );
  }
}

class _backgroundWhile extends StatelessWidget {
  const _backgroundWhile({
    Key? key,
    required this.height,
    required this.width,
  }) : super(key: key);

  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: height * 0.32,
      left: 0,
      width: width,
      height: height,
      child: Container(
        width: 80,
        height: 80,
        color: kPrimaryTextColor,
      ),
    );
  }
}

class _myInformation extends StatelessWidget {
  const _myInformation({
    Key? key,
    required this.width,
  }) : super(key: key);

  final double width;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 100,
      left: 0,
      height: 80,
      width: width,
      child: Container(
        width: width,
        height: 80,
        margin: const EdgeInsets.only(
            left: kDefaultPadding, right: kDefaultPadding),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: kPrimaryTextColor,
        ),
        child: Container(
          child: Row(
            children: [
              const SizedBox(
                width: 20,
              ),
              // avatar
              CircleAvatar(
                radius: 30,
                backgroundImage:
                    AssetImage(Get.arguments['img'],),
              ),
              const SizedBox(
                width: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // name
                  TextBold(
                    text: Get.arguments['name'],
                    size: 18,
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  // level
                  TextNormal(
                    text: "Top Level",
                    color: kButtonColor,
                    size: 16,
                  )
                ],
              ),
              Expanded(child: Container()),
              // notification
              Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: kPrimaryTextColor),
                child: const Center(
                  child: Icon(
                    Icons.notifications,
                    color: kMainColor,
                    size: 32,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _myBackHome extends StatelessWidget {
  const _myBackHome({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 50,
        left: 10,
        child: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.home,
            color: kPrimaryTextColor,
          ),
        ));
  }
}
