import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_getx/view/recent_recent_page.dart';
import 'package:flutter_getx/consutu.dart';
import 'package:flutter_getx/view/widgets/app_text_bold.dart';
import 'package:flutter_getx/view/widgets/app_text_normal.dart';

import 'package:get/get.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List list = [];
  List info = [];

  _readDate() async {
    await DefaultAssetBundle.of(context)
        .loadString("assets/json/recent.json")
        .then((s) {
      setState(() {
        list = json.decode(s);
      });
    });

    await DefaultAssetBundle.of(context)
        .loadString("assets/json/detail.json")
        .then((s) {
      setState(() {
        info = json.decode(s);
      });
    });
  }

  @override
  void initState() {
    _readDate();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: kMainColor,
        height: double.maxFinite,
        width: double.maxFinite,
        child: Container(
            padding: const EdgeInsets.only(top: 50),
            child: Column(
              children: [
                _myInformation(),
                Container(
                  child: Column(
                    children: [
                      // popular cntest
                      Container(
                        padding: const EdgeInsets.only(
                            left: kDefaultPadding, right: kDefaultPadding),
                        margin: const EdgeInsets.only(top: 20),
                        child: Row(
                          children: [
                            TextBold(
                              text: "Popular Contest",
                              size: 18,
                            ),
                            Expanded(child: Container()),
                            Container(
                              child: Row(
                                children: [
                                  TextNormal(
                                    text: "See more",
                                    size: 12,
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Container(
                                    width: 46,
                                    height: 46,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: kButtonColor),
                                    child: GestureDetector(),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      // list popular contest
                      Container(
                        height: 170,
                        child: PageView.builder(
                            controller: PageController(viewportFraction: 0.88),
                            itemCount: info.length,
                            itemBuilder: (_, index) {
                              return GestureDetector(
                                onTap: (() {
                                  Get.toNamed("/detail/", arguments: {
                                    'name': info[index]['name'].toString(),
                                    'title': info[index]['title'].toString(),
                                    'text': info[index]['text'].toString(),
                                    'img': info[index]['img'].toString(),
                                    'time': info[index]['time'].toString(),
                                    'prize': info[index]['prize'].toString(),
                                  });
                                }),
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 10),
                                  height: 170,
                                  width: double.maxFinite,
                                  margin: const EdgeInsets.only(right: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      color: index.isEven
                                          ? kBlueColor
                                          : kVioletColor),
                                  child: Column(
                                    children: [
                                      Container(
                                          child: Row(
                                        children: [
                                          TextBold(
                                            text: info[index]['title'],
                                            size: 24,
                                            color: kPrimaryTextColor,
                                          ),
                                          Expanded(child: Container())
                                        ],
                                      )),
                                      const SizedBox(height: 5),
                                      Container(
                                        width: double.maxFinite,
                                        child: TextNormal(
                                          text: info[index]['text'],
                                          size: 16,
                                          color: kGreyTextColor,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      const Divider(
                                        thickness: 1.0,
                                      ),
                                      Row(children: [
                                        for (int index = 0; index < 4; index++)
                                          Container(
                                            width: 36,
                                            height: 36,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(24),
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                        info[index]['img'],
                                                      ),
                                                      fit: BoxFit.cover)),
                                            ),
                                          )
                                      ]),
                                    ],
                                  ),
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                ),
                _recentContest(list: list)
              ],
            )),
      ),
    );
  }
}

class _recentContest extends StatelessWidget {
  const _recentContest({
    Key? key,
    required this.list,
  }) : super(key: key);

  final List list;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        child: Column(
          children: [
            // recent cntest
            Container(
              padding: const EdgeInsets.only(
                  left: kDefaultPadding, right: kDefaultPadding),
              margin: const EdgeInsets.only(top: 20),
              child: Row(
                children: [
                  TextBold(
                    text: "Recent Contest",
                    size: 18,
                  ),
                  Expanded(child: Container()),
                  Container(
                    child: Row(
                      children: [
                        TextNormal(
                          text: "See more",
                          size: 12,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Container(
                          width: 46,
                          height: 46,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(10),
                              color: kButtonColor),
                          //child: AppIcon(source: "arrow_forward_ios_sharp", color: kPrimaryTextColor),
                          child: IconButton(
                              onPressed: () {
                                Get.to(RecentRecentPage());
                              },
                              icon: const Icon(
                                Icons.arrow_forward_ios_sharp,
                                color: kPrimaryTextColor,
                              )),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            // list
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  padding: const EdgeInsets.only(
                      left: kDefaultPadding,
                      right: kDefaultPadding),
                  child: Expanded(
                      child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: list.length,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: const EdgeInsets.only(bottom: 10),
                        child: Container(
                          height: 80,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: kPrimaryTextColor,
                          ),
                          child: Row(
                            children: [
                              const SizedBox(
                                width: 20,
                              ),
                              // image
                              CircleAvatar(
                                radius: 30,
                                backgroundImage: AssetImage(
                                    list[index]['img']),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.center,
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: [
                                  TextNormal(
                                    text: list[index]['status'],
                                    color: kButtonColor,
                                    size: 16,
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  SizedBox(
                                    width: 150,
                                    child: TextNormal(
                                      text: list[index]['text'],
                                      size: 18,
                                    ),
                                  ),
                                ],
                              ),
                              Expanded(child: Container()),
                              // notification
                              Container(
                                width: 70,
                                height: 70,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.circular(20),
                                    color: kPrimaryTextColor),
                                child: Text(list[index]['time'], style: TextStyle(fontSize: 12),),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  )),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class _myInformation extends StatelessWidget {
  const _myInformation({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:
          const EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding),
      child: Container(
        height: 80,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: kPrimaryTextColor,
        ),
        child: Row(
          children: [
            const SizedBox(
              width: 20,
            ),
            // avatar
            const CircleAvatar(
              radius: 30,
              backgroundImage: AssetImage("assets/image/background.jpg"),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // name
                TextBold(
                  text: "James Smith",
                  size: 18,
                ),
                const SizedBox(
                  height: 5,
                ),
                // level
                TextNormal(
                  text: "Top Level",
                  color: kButtonColor,
                  size: 16,
                )
              ],
            ),
            Expanded(child: Container()),
            // notification
            Container(
              width: 70,
              height: 70,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: kPrimaryTextColor),
              child: const Center(
                child: Icon(
                  Icons.notifications,
                  color: kMainColor,
                  size: 32,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
