import 'package:flutter/material.dart';

class TextNormal extends StatelessWidget {
  double size;
  final String text;
  final Color color;

  TextNormal(
      {Key? key, this.size = 15, required this.text, this.color = Colors.black54})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style:
          TextStyle(fontSize: size, color: color),
    );
  }
}