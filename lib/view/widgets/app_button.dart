import 'package:flutter/cupertino.dart';

import 'app_text_bold.dart';


class ButtonApp extends StatelessWidget {
  double height;
  double width;
  double border;
  final String text;
  final Color textColor;
  final double textSize;
  final Color background;

  ButtonApp(
      {Key? key,
      this.height = 50,
      this.width = double.maxFinite,
      this.border = 10,
      required this.text,
      required this.textColor,
      required this.textSize,
      required this.background})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: const EdgeInsets.only(top: 10, bottom: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(border), color: background),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextBold(
            text: text,
            size: textSize,
            color: textColor,
          ),
        ],
      ),
    );
  }
}
