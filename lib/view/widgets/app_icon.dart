import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppIcon extends StatelessWidget {
  double height;
  double width;
  final String source;
  // final Color color;
  // AppIcon(
  //     {Key? key,
  //     this.height = 24,
  //     this.width = 24,
  //     required this.source,
  //     required this.color})
  //     : super(key: key);

  AppIcon(
      {Key? key,
      this.height = 24,
      this.width = 24,
      required this.source})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: SvgPicture.asset(source),
    );
  }
}
