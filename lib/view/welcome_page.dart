import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_getx/view/home_page.dart';
import 'package:flutter_getx/consutu.dart';
import 'package:flutter_getx/view/widgets/app_text_bold.dart';
import 'package:flutter_getx/view/widgets/app_text_normal.dart';
import 'package:get/get.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: Column(
          children: [
            Container(
              height: 500,
              width: double.maxFinite,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/image/background.jpg"),
                      fit: BoxFit.cover)),
            ),
            Expanded(
              child: Container(
                width: double.maxFinite,
                decoration: const BoxDecoration(color: kMainColor),
                // padding: EdgeInsets.only(left: kDefaultPadding),
                child: Container(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextBold(
                        text: "Pick Your Favorite \nContests ",
                        size: 24,
                        color: kPrimaryTextColor,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                          width: 300,
                          child: TextNormal(
                            text:
                                "We make great design work happen with our great community designer",
                            size: 16,
                            color: kGreyTextColor,
                          )),
                      const SizedBox(
                        height: 20,
                      ),

                      InkWell(
                        onTap: (() {
                          Get.to(HomePage());
                        }),
                        child: Container(
                          width: 180,
                          height: 50,
                          decoration: const BoxDecoration(color: kButtonColor, borderRadius: BorderRadius.all(Radius.circular(20))),
                          child: Center(child: TextNormal(text: "Get started", color: kPrimaryTextColor,)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
