import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_getx/view/detail_page.dart';
import 'package:flutter_getx/consutu.dart';
import 'package:get/get.dart';

import 'package:flutter_getx/view/widgets/app_text_bold.dart';
import 'package:flutter_getx/view/widgets/app_text_normal.dart';
class RecentRecentPage extends StatefulWidget {
  const RecentRecentPage({Key? key}) : super(key: key);

  @override
  State<RecentRecentPage> createState() => _RecentRecentPageState();
}

class _RecentRecentPageState extends State<RecentRecentPage> {
  List info = [];

  _readDate() async {
    await DefaultAssetBundle.of(context)
        .loadString("assets/json/detail.json")
        .then((s) {
      setState(() {
        info = json.decode(s);
      });
    });
  }

  @override
  void initState() {
    _readDate();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kMainColor,
        // back home
        leading: InkWell(
            onTap: () {
              Get.back();
            },
            child: Icon(Icons.chevron_left)),
      ),
      // list 
      body: Container(
        decoration: const BoxDecoration(color: kBackgroundRecentContest),
        child: Expanded(
            child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  padding: const EdgeInsets.only(
                      left: kDefaultPadding, right: kDefaultPadding),
                  child: Expanded(
                      child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: info.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: (() {
                          Get.toNamed("/detail/", arguments: {
                                    'name': info[index]['name'].toString(),
                                    'title': info[index]['title'].toString(),
                                    'text': info[index]['text'].toString(),
                                    'img': info[index]['img'].toString(),
                                    'time': info[index]['time'].toString(),
                                    'prize': info[index]['prize'].toString(),
                                  });
                        }),
                        child: Container(
                          padding: const EdgeInsets.only(
                              left: 10, right: 10, top: 10),
                          height: 170,
                          width: double.maxFinite,
                          margin: const EdgeInsets.only(top: 10, bottom: 5),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: index.isEven ? kBlueColor : kVioletColor),
                          child: Column(
                            children: [
                              Container(
                                  child: Row(
                                children: [
                                  TextBold(
                                    text: info[index]['title'],
                                    size: 24,
                                    color: kPrimaryTextColor,
                                  ),
                                  Expanded(child: Container())
                                ],
                              )),
                              const SizedBox(height: 5),
                              Container(
                                width: double.maxFinite,
                                child: TextNormal(
                                  text:
                                      info[index]['text'],
                                  size: 16,
                                  color: kGreyTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              const Divider(
                                thickness: 1.0,
                              ),
                              Row(children: [
                                for (int i = 0; i < 4; i++)
                                  Container(
                                    width: 36,
                                    height: 36,
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(24),
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  info[i]['img']),
                                              fit: BoxFit.cover)),
                                    ),
                                  )
                              ]),
                            ],
                          ),
                        ),
                      );
                    },
                  )),
                ),
              ),
            ),
          ],
        )),
      ),
    );
  }
}
