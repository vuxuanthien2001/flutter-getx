import 'package:flutter_getx/route_management/routes.dart';
import 'package:get/get.dart';

import '../view/detail_page.dart';
import '../view/welcome_page.dart';

class AllPages {
  static List<GetPage> getPages() {
    return [
      GetPage(name: kWelcome, page: () => WelcomePage()),
      GetPage(name: kDetail, page: () => DetailPage()),
    ];
  }
}
