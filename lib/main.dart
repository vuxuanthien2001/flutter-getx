import 'package:flutter/material.dart';
import 'package:flutter_getx/route_management/all_pages.dart';
import 'package:flutter_getx/route_management/routes.dart';
import 'package:flutter_getx/route_management/screen_bindings.dart';
import 'package:flutter_getx/view/detail_page.dart';
import 'package:flutter_getx/view/welcome_page.dart';
import 'package:get/get.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: kWelcome,
      getPages: AllPages.getPages(),
      initialBinding: ScreenBindings(),
      //home: WelcomePage(),
    );
  }
}

