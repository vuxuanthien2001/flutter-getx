import 'package:flutter/material.dart';

const kPrimaryTextColor = Color.fromARGB(255, 255, 255, 255);
const kWhiteTextColor = Color.fromARGB(253, 254, 255, 255);
const kBlackTextColor = Color.fromARGB(255, 255, 255, 255);
const kGreyTextColor = Color.fromARGB(255, 213, 211, 211);
const kMainColor = Color.fromRGBO(94, 188, 220, 800);
const kButtonColor = Color.fromRGBO(253, 186, 55, 800);
const kVioletColor = Color.fromRGBO(136, 136, 199, 800);
const kBlueColor = Color.fromRGBO(95, 189, 221, 800);
const kBackgroundRecentContest = Color.fromRGBO(197, 226, 246, 800);




const kDefaultPadding = 20.0;